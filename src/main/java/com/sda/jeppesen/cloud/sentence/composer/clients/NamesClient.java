package com.sda.jeppesen.cloud.sentence.composer.clients;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "jeppesen-cloud-names") // component
public interface NamesClient {

    @GetMapping("/name")
    public String getName();
}
