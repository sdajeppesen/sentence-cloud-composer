package com.sda.jeppesen.cloud.sentence.composer.clients;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "jeppesen-cloud-numerator") // component
public interface NumeratorClient {

    @GetMapping("/number")
    public String getNumber();
}
