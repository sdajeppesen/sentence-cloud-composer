package com.sda.jeppesen.cloud.sentence.composer.controller;

import com.sda.jeppesen.cloud.sentence.composer.clients.NamesClient;
import com.sda.jeppesen.cloud.sentence.composer.clients.NumeratorClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SentenceController {

    @Autowired
    private NamesClient namesClient;
    @Autowired
    private NumeratorClient numeratorClient;

    @Value("${jeppesen.cloud.sentence}")
    private String sentence; // niezmienny

    @GetMapping("/sentence")
    public String returnSentence(){

        String slowoZInnegoSerwisu = namesClient.getName(); //... pobranie słowa z serwisu "names"
        String liczebnikZInnegoSerwisu = numeratorClient.getNumber(); //... pobranie słowa z serwisu "names"

        String zdanieZZamienionymSlowem = sentence.replace("{NAME}", slowoZInnegoSerwisu);
        zdanieZZamienionymSlowem = zdanieZZamienionymSlowem.replace("{NUMBER}", liczebnikZInnegoSerwisu);

        return zdanieZZamienionymSlowem;
    }

}

// user-service                 //3
// authorization // logowanie+sesja //1
// student-service //           //2
// class-service (zajęcia) //   //5